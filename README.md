# Unique Digit Calculator

### Calculador de Dígitos Únicos

Este projeto visa realizar cálculos de dígitos únicos a partir de uma 
sequência numérica e um fator de multiplicação dos valores.

A documentação completa da API está disponível no formato Open API no arquivo `swagger.yml` 
na raiz do projeto.

Este arquivo pode ser visualizado de forma amigável em 
[http://editor.swagger.io](http://editor.swagger.io).
Esta documentação também pode ser vista direto no projeto, quando estiver sendo executado. 

## Instruções

### Como executar o projeto
**ATENÇÃO:** Para executar, é necessário que Java 11 esteja corretamente instalado
em seu sistema, bem como o Maven

1. Clonar ou realizar o download do repositório
2. Acessar o diretório do projeto
3. No terminal, executar o comando `mvn compile` para baixar as dependências 
e compilar o projeto.
4. Executar o comando `mvn org.springframework.boot:spring-boot-maven-plugin:run` para executar o projeto.

A aplicação irá então iniciar na porta `8080` e as rotas disponíveis na documentação
poderão ser acessadas normalmente.

Para acessar a documentação do *Swagger* dentro da aplicação, acesse no navegador a 
URL [http://localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html)

### Como executar os testes do projeto
1. No terminal, executar o comando `mvn test` para executar os testes unitários.
2. No Postman, importar o arquivo `postman_collection.json`, abrir o *Collection Runner*, 
selecionar o *Unique Digits API* e clicar em *Run ...*.

#### Recursos utilizados
* Java 11 (Open JDK 11)
* Spring Boot
* Maven
* JUnit 5
* H2 In-Memory Database
