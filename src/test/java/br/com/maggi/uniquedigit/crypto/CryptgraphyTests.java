package br.com.maggi.uniquedigit.crypto;

import br.com.maggi.uniquedigit.cryptography.Cryptography;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;

@SpringBootTest
public class CryptgraphyTests {

    @Test
    @DisplayName("Should generate public and private keys with success")
    public void ShouldGenerateKeysWithSuccess () {
        KeyPair keyPair = Cryptography.GenerateKeys();
        Assertions.assertNotNull(keyPair.getPrivate());
        Assertions.assertNotNull(keyPair.getPublic());
    }

    @Test
    @DisplayName("Should decrypt some text previously encrypted")
    public void ShouldDecryptWithSuccess () {
        KeyPair keyPair = Cryptography.GenerateKeys();
        PublicKey pubKey = keyPair.getPublic();
        PrivateKey privKey = keyPair.getPrivate();

        String expectedText = "#sangueLaranja";

        String encryptedText = Cryptography.Encrypt(expectedText, pubKey);
        String decryptedText = Cryptography.Decrypt(encryptedText, privKey);

        Assertions.assertEquals(expectedText, decryptedText);

    }
}
