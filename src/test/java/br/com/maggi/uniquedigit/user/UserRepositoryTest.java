package br.com.maggi.uniquedigit.user;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class UserRepositoryTest {

    @Autowired
    UserRepository repository;

    @Test
    @DisplayName("Should return an inserted user from Db")
    public void ShouldReturnUserFromDb() {
        String name = "John";
        String email = "test@gmail.com";

        User userToAdd = new User(name, email);
        repository.save(userToAdd);

        User user = repository.findByEmail(email);

        Assertions.assertNotNull(user);
    }
}
