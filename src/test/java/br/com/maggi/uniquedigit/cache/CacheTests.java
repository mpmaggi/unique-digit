package br.com.maggi.uniquedigit.cache;

import br.com.maggi.uniquedigit.core.UniqueDigit;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class CacheTests {

    private final String numberSequence = "9875";
    private final Integer multiplier = 4;

    @Autowired
    Cache cache;

    private UniqueDigit uniqueDigit;

    @BeforeEach
    public void resetValues() {
        uniqueDigit = new UniqueDigit(numberSequence,multiplier);
    }

    @Test
    @DisplayName("Should add a new Unique Digit object to cache")
    public void ShouldAddUniqueDigitToCache() {
        cache.Add(uniqueDigit);

        var result = cache.Get(numberSequence, multiplier);

        Assertions.assertEquals(result, uniqueDigit);
    }

    @Test
    @DisplayName("Should not put duplicated keys on cache")
    public void ShouldNotPutDuplicated() {
        var uniqueDigit2 = new UniqueDigit(numberSequence, multiplier);

        cache.Add(uniqueDigit);
        cache.Add(uniqueDigit2);

        Assertions.assertEquals(1, cache.getSize());
    }

    @Test
    @DisplayName("Should not exceeds the size of 10 on cache, removing the eldest element")
    public void ShouldNotExceedLimitSize() {
        cache.Add(uniqueDigit);
        var exists = cache.Get(uniqueDigit.getNumericSequence(), uniqueDigit.getMultiplier());
        Assertions.assertNotNull(exists);

        for (int i = 0; i < 10; i++) {
            var numericSequence = new StringBuilder("10082020").append(i).toString();

            var digit = new UniqueDigit(numericSequence, multiplier);
            cache.Add(digit);
        }

        var existsFirstInserted = cache.Get(uniqueDigit.getNumericSequence(), uniqueDigit.getMultiplier());

        Assertions.assertEquals(10, cache.getSize());
        Assertions.assertNull(existsFirstInserted);

    }

}