package br.com.maggi.uniquedigit;

import br.com.maggi.uniquedigit.core.UniqueDigitUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class UniqueDigitUtilsTests {

    @Test
    @DisplayName("Should validate with success the input number")
    public void ShouldValidateWithSuccessInputNumber() {
        int number = Integer.MAX_VALUE;
        boolean isValidNumber = UniqueDigitUtils.isValidNumber(String.valueOf(number));
        Assertions.assertTrue(isValidNumber);
    }

    @Test
    @DisplayName("Should not validate the input number because its negative")
    public void ShouldNotValidateNegativeInputNumber() {
        boolean isValidNumber = UniqueDigitUtils.isValidNumber("-10");
        Assertions.assertFalse(isValidNumber);
    }

    @Test
    @DisplayName("Should not validate the input number because its out of allowed range")
    public void ShouldNotValidateInputNumberOutOfRange() {
        long bigNumber = Long.MAX_VALUE;
        boolean isValidNumber = UniqueDigitUtils.isValidNumber(String.valueOf(bigNumber));
        Assertions.assertFalse(isValidNumber);
    }

    @Test
    @DisplayName("Should not validate the input number because its not a number")
    public void ShouldNotValidateAlphanumericChars() {
        boolean isValidNumber = UniqueDigitUtils.isValidNumber("a0120b");
        Assertions.assertFalse(isValidNumber);
    }

    @Test
    @DisplayName("Should validate with success the multiplier factor")
    public void ShouldValidateWithSuccessMultiplier() {
        boolean isValidMultiplier = UniqueDigitUtils.isValidMultiplierFactor(50);
        Assertions.assertTrue(isValidMultiplier);
    }

    @Test
    @DisplayName("Should not validate the multiplier factor because its negative")
    public void ShouldNotValidateTheMultiplierNegative() {
        boolean isValidMultiplier = UniqueDigitUtils.isValidMultiplierFactor(-10);
        Assertions.assertFalse(isValidMultiplier);
    }

    @Test
    @DisplayName("Should not validate the multiplier factor because its out of range")
    public void ShouldNotValidateTheMultiplierOutOfRange() {
        boolean isValidMultiplier = UniqueDigitUtils.isValidMultiplierFactor(100001);
        Assertions.assertFalse(isValidMultiplier);
    }

    @Test
    @DisplayName("Should not validate because number is out of range")
    public void ShouldNotValidateNisOutOfRange() {
        String n = "2147483648";
        Integer k = 3;
        String validateMessage;

        validateMessage = UniqueDigitUtils.validateInput(n, k);
        Assertions.assertEquals("Invalid input number. Only 0-9 chars are accepted.", validateMessage);
    }


    @Test
    @DisplayName("Should not validate because multiplier is out of range")
    public void ShouldNotValidateKisOutOfRange() {
        String n = "2147483647";
        Integer k = 100001;
        String validateMessage;

        validateMessage = UniqueDigitUtils.validateInput(n, k);
        Assertions.assertEquals("Invalid multiplier factor. Only numbers between 1 and 100000 are accepted.", validateMessage);
    }

    @Test
    @DisplayName("Should not validate because both number and multiplier are out of range")
    public void ShouldNotValidateAllInputsAreOutOfRange() {
        String n = "-401";
        Integer k = 100001;
        String validateMessage;

        validateMessage = UniqueDigitUtils.validateInput(n, k);
        Assertions.assertEquals("Invalid input number. Only 0-9 chars are accepted.Invalid multiplier factor. Only numbers between 1 and 100000 are accepted.",
                validateMessage);
    }
}