package br.com.maggi.uniquedigit;

import br.com.maggi.uniquedigit.cache.Cache;
import br.com.maggi.uniquedigit.core.IUniqueDigit;
import br.com.maggi.uniquedigit.core.UniqueDigit;
import br.com.maggi.uniquedigit.core.UniqueDigitService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class UniqueDigitServiceTests {

    private final String numberSequence = "9875";
    private final Integer multiplier = 4;

    @Autowired
    Cache cache;

    @Autowired
    UniqueDigitService service;

    @Test
    @DisplayName("Should calculate the unique digit and add it to cache")
    public void ShouldCalculateWithSuccessAndAddToCache() {
        IUniqueDigit digit = service.Calculate(numberSequence, multiplier);
        UniqueDigit digitFromCache = cache.Get(digit.getNumericSequence(), digit.getMultiplier());

        Assertions.assertEquals(numberSequence, digit.getNumericSequence());
        Assertions.assertEquals(multiplier, digit.getMultiplier());
        Assertions.assertEquals(digit, digitFromCache);
    }

    @Test
    @DisplayName("Should not calculate because invalid inputs")
    public void ShouldNotCalculateWithInvalidInputs() {
        String numberSequence = "-401";
        Integer multiplier = 100001;

        Exception exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            IUniqueDigit digit = service.Calculate(numberSequence, multiplier);
        });

        UniqueDigit digitFromCache = cache.Get(numberSequence, multiplier);

        String expectedMessage = "Invalid input number. Only 0-9 chars are accepted.Invalid multiplier factor. Only numbers between 1 and 100000 are accepted.";

        Assertions.assertNull(digitFromCache);
        Assertions.assertEquals(expectedMessage, exception.getMessage());
    }
}