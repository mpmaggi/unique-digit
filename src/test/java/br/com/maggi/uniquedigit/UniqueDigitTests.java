package br.com.maggi.uniquedigit;

import br.com.maggi.uniquedigit.core.UniqueDigit;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class UniqueDigitTests {

    @Test
    @DisplayName("Should calculate the right unique digit based on the inputs. The input number is 1 char length")
    public void ShouldCalculateWithSuccessNumberLessThan10() {
        String n = "2";
        Integer k = 1;
        Integer expectedResult = 2;

        UniqueDigit digit = new UniqueDigit(n, k);
        Assertions.assertEquals(expectedResult, digit.getCalculatedDigit());

    }

    @Test
    @DisplayName("Should calculate the right unique digit based on the inputs.  The input number is 4 char length")
    public void ShouldCalculateWithSuccessNumberGreaterThan10() {
        String n = "9875";
        Integer k = 4;
        Integer expectedResult = 8;

        UniqueDigit digit = new UniqueDigit(n, k);
        Assertions.assertEquals(expectedResult, digit.getCalculatedDigit());

    }

    @Test
    @DisplayName("Should not calculate because number is out of range")
    public void ShouldNotCalculateNisOutOfRange() {
        String n = "2147483648";
        Integer k = 3;
        UniqueDigit digit = null;

        try {
            digit = new UniqueDigit(n, k);
        } catch (Exception ex) {
            Assertions.assertEquals("Invalid input number. Only 0-9 chars are accepted.", ex.getMessage());
        }

        Assertions.assertNull(digit);

    }


    @Test
    @DisplayName("Should not calculate because multiplier is out of range")
    public void ShouldNotCalculateKisOutOfRange() {
        String n = "2147483647";
        Integer k = 100001;
        UniqueDigit digit = null;

        try {
            digit = new UniqueDigit(n, k);
        } catch (Exception ex) {
            Assertions.assertEquals("Invalid multiplier factor. Only numbers between 1 and 100000 are accepted.", ex.getMessage());
        }

        Assertions.assertNull(digit);
    }

    @Test
    @DisplayName("Should not calculate because both number and multiplier are out of range")
    public void ShouldNotCalculateAllInputsAreOutOfRange() {
        String n = "-401";
        Integer k = 100001;
        UniqueDigit digit = null;

        try {
            digit = new UniqueDigit(n, k);
        } catch (Exception ex) {
            Assertions.assertEquals("Invalid input number. Only 0-9 chars are accepted.Invalid multiplier factor. Only numbers between 1 and 100000 are accepted.",
                    ex.getMessage());
        }

        Assertions.assertNull(digit);
    }

}
