DROP TABLE IF EXISTS users_digits;
DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS digits;
DROP TABLE IF EXISTS keys;

CREATE TABLE users (
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  email VARCHAR(250) NOT NULL,
  name VARCHAR(250) NOT NULL
);

CREATE table digits (
  id INT AUTO_INCREMENT PRIMARY KEY,
  numeric_sequence VARCHAR(10) NOT NULL,
  multiplier INT NOT NULL,
  calculated_digit INT NOT NULL
);

CREATE table users_digits (
  user_id VARCHAR(250) NOT NULL,
  digit_id INT NOT NULL
);

CREATE TABLE keys (
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  public_key TEXT NOT NULL,
  private_key TEXT NOT NULL
);
