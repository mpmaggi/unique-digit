package br.com.maggi.uniquedigit.cache;

import br.com.maggi.uniquedigit.core.UniqueDigit;
import br.com.maggi.uniquedigit.core.UniqueDigitKey;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;
import java.util.Map;

@Service
public class Cache {

    private static final int LIMITSIZE = 10;

    private Map<UniqueDigitKey, UniqueDigit> cache = new LinkedHashMap<UniqueDigitKey, UniqueDigit>() {
        @Override
        protected boolean removeEldestEntry(final Map.Entry eldest) {
            return size() > LIMITSIZE;
        }
    };

    public void Add(UniqueDigit digit) {
        cache.put(digit.getKey(),digit);
    }

    public UniqueDigit Get(String numberSequence, Integer multiplier) {
        var key = new UniqueDigitKey(numberSequence, multiplier);
        var exists = cache.containsKey(key);

        if (exists) {
            return cache.get(key);
        } else {
            return null;
        }

    }

    public int getSize() {
        return cache.size();
    }

}
