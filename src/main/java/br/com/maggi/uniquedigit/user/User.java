package br.com.maggi.uniquedigit.user;

import br.com.maggi.uniquedigit.core.UniqueDigit;
import br.com.maggi.uniquedigit.cryptography.Cryptography;
import br.com.maggi.uniquedigit.core.UniqueDigitKey;

import javax.persistence.*;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Integer id;

    @Column
    private String name;

    @Column
    private String email;

    @ManyToMany
    @JoinTable(name="users_digits",
        joinColumns = {@JoinColumn(name="user_id")},
        inverseJoinColumns = {@JoinColumn(name="digit_id")})
    private List<UniqueDigit> digits;

    private User () {}

    public User (String name, String email) {
        this.name = name;
        this.email = email;
        digits = new ArrayList<>();
    }

    public void AddUniqueDigit(UniqueDigit uniqueDigit) {
        if(!DigitExists(uniqueDigit.getKey())) {
            this.digits.add(uniqueDigit);
        }
    }

    public Boolean DigitExists(UniqueDigitKey key) {
        var exists = this.digits
                .stream()
                .filter(digit -> digit.getKey().equals(key))
                .findAny()
                .orElse(null);

        return exists != null;
    }

    public static User Encrypt(User user, String authToken) {
        PublicKey pubKey = Cryptography.Base64ToPublicKey(authToken);
        user.setName(Cryptography.Encrypt(user.getName(), pubKey));
        user.setEmail(Cryptography.Encrypt(user.getEmail(), pubKey));

        return user;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<UniqueDigit> getDigits() {
        return digits;
    }

}
