package br.com.maggi.uniquedigit.user;

import br.com.maggi.uniquedigit.core.UniqueDigit;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
@Api("Users")
public class UserControllers {

    @Autowired
    UserRepository repository;

    @PostMapping
    @ApiOperation("Create a new User")
    @ApiResponses({
            @ApiResponse(code = 409, message = "User already exists"),
            @ApiResponse(code = 500, message = "An error occurred trying to save User"),
            @ApiResponse(code = 201, message = "User successfully created")
    })
    public ResponseEntity saveUser(@RequestBody User user) {
        User newUser;

        if (repository.findByEmail(user.getEmail()) != null) {
            return new ResponseEntity("User already exists", HttpStatus.CONFLICT);
        }

        try {
            newUser = repository.save(user);
        } catch (Exception ex) {
          return new ResponseEntity(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity(null, HttpStatus.CREATED);

    }

    @GetMapping
    @ApiOperation("Return all users")
    @ApiResponses({
            @ApiResponse(code = 204, message = "There is no User to list"),
            @ApiResponse(code = 200, message = "Users found to list")
    })
    public ResponseEntity getAllUsers() {
        List<User> users = repository.findAll();

        if (users.size() == 0) {
            return new ResponseEntity(null, HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity(users, HttpStatus.OK);

    }

    @GetMapping("/{id}")
    @ApiOperation("Return a User by Id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "User found"),
            @ApiResponse(code = 404, message = "User not found")
    })
    public ResponseEntity getUser(@PathVariable("id") Integer id, @RequestHeader(name = "Authorization") String authToken) {
        User user = repository.findById(id).orElse(null);

        if (user != null) {
            User encryptedUser = User.Encrypt(user, authToken);

            return new ResponseEntity(encryptedUser, HttpStatus.OK);
        }
        return new ResponseEntity(null, HttpStatus.NOT_FOUND);
    }


    @DeleteMapping("/{id}")
    @ApiOperation("Delete a User by Id")
    @ApiResponses({
            @ApiResponse(code = 404, message = "User not found"),
            @ApiResponse(code = 500, message = "Error on trying to delete User"),
            @ApiResponse(code = 204, message = "User successfully deleted")
    })
    public ResponseEntity deleteUser(@PathVariable("id") Integer id) {
        User user = repository.findById(id).orElse(null);

        if (user == null) {
            return new ResponseEntity(null, HttpStatus.NOT_FOUND);
        }

        try {
            repository.delete(user);
        } catch (Exception ex) {
            return new ResponseEntity(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity(null, HttpStatus.NO_CONTENT);
    }

    @PutMapping("/{id}")
    @ApiOperation("Update User data")
    @ApiResponses({
            @ApiResponse(code = 404, message = "User not found"),
            @ApiResponse(code = 200, message = "User successfully updated")
    })
    public ResponseEntity editUser(@PathVariable("id") Integer id, @RequestBody User user) {
        User userFromDb = repository.findById(id).orElse(null);

        if (userFromDb == null) {
            return new ResponseEntity(null, HttpStatus.NOT_FOUND);
        }

        userFromDb.setName(user.getName());
        userFromDb.setEmail(user.getEmail());
        repository.save(userFromDb);

        return new ResponseEntity(null, HttpStatus.OK);
    }

    @GetMapping("/{id}/uniqueDigits")
    @ApiOperation("Return all calculated digits from a User")
    @ApiResponses({
            @ApiResponse(code = 404, message = "User not found"),
            @ApiResponse(code = 204, message = "No digits found for this User"),
            @ApiResponse(code = 200, message = "Unique Digits found for this User", response = UniqueDigit.class)
    })
    public ResponseEntity getDigitsFromUser(@PathVariable Integer id) {
        User user = repository.findById(id).orElse(null);

        if (user == null) {
            return new ResponseEntity(null, HttpStatus.NOT_FOUND);
        }

        if(user.getDigits().size() == 0) {
            return new ResponseEntity(null, HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity(user.getDigits(), HttpStatus.OK);
    }

}
