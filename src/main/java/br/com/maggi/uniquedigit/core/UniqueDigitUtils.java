package br.com.maggi.uniquedigit.core;

public class UniqueDigitUtils {

    public static boolean isValidNumber(String number) {
        try {
            int parsedNumber = Integer.parseInt(number);
            if (parsedNumber > 0) {
                return true;
            }
            return false;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static boolean isValidMultiplierFactor(Integer k) {
        Integer maxValue = (int)Math.pow(10, 5);
        if (k > 0 && k < maxValue) {
            return true;
        }
        return false;
    }

    public static String validateInput(String numberSequence, Integer multiplier) {
        var validNumber = UniqueDigitUtils.isValidNumber(numberSequence);
        var validMultiplier = UniqueDigitUtils.isValidMultiplierFactor(multiplier);

        var errorMessage = new StringBuilder();

        if (!validNumber) {
            errorMessage.append("Invalid input number. Only 0-9 chars are accepted.");
        }

        if (!validMultiplier) {
            errorMessage.append("Invalid multiplier factor. Only numbers between 1 and 100000 are accepted.");
        }

        return errorMessage.toString();
    }

    public static String repeatNumber (UniqueDigitKey key) {
        var fullString = new StringBuilder(key.getNumericSequence());

        for (int i = 1; i < key.getMultiplier(); i++) {
            fullString.append(key.getNumericSequence());
        }

        return fullString.toString();
    }

}
