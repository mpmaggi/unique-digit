package br.com.maggi.uniquedigit.core;

import java.io.Serializable;

public class UniqueDigitKey implements Serializable {
    private String numericSequence;
    private Integer multiplier;

    public UniqueDigitKey(String numericSequence, Integer multiplier) {
        this.numericSequence = numericSequence;
        this.multiplier = multiplier;
    }

    public String getNumericSequence() {
        return this.numericSequence;
    }

    public Integer getMultiplier() {
        return this.multiplier;
    }

    public String toString() {
        return new StringBuilder(this.getClass().getName())
                .append("#")
                .append(this.numericSequence)
                .append("#")
                .append(this.multiplier)
                .toString();
    }

    public int hashCode() {
        return this.toString().hashCode();
    }

    public boolean equals(Object key) {
        if(key == null || this.getClass() != key.getClass()) {
            return false;
        }
        return this.toString().equals(key.toString());

    }



}
