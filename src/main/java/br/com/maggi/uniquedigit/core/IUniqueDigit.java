package br.com.maggi.uniquedigit.core;

public interface IUniqueDigit {
    public String getNumericSequence();
    public Integer getMultiplier();
    public Integer getCalculatedDigit();

    public void doCalculate();
}
