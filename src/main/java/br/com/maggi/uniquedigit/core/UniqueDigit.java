package br.com.maggi.uniquedigit.core;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name="digits")
public class UniqueDigit implements IUniqueDigit{

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Integer id;

    @Column(name = "numeric_sequence")
    private String numericSequence;

    @Column
    private Integer multiplier;

    @Column(name = "calculated_digit")
    private Integer calculatedDigit;

    @Transient
    @JsonIgnore
    private UniqueDigitKey key;

    private UniqueDigit() {}

    public UniqueDigit(String n, Integer k) throws IllegalArgumentException{
        var validation = UniqueDigitUtils.validateInput(n, k);
        if (!validation.isBlank()) { throw new IllegalArgumentException(validation); }

        this.numericSequence = n;
        this.multiplier = k;
        this.key = new UniqueDigitKey(this.numericSequence, this.multiplier);

        doCalculate();
    }

    public String getNumericSequence() { return this.numericSequence; }
    public Integer getMultiplier() { return this.multiplier; }
    public Integer getCalculatedDigit() { return this.calculatedDigit; }
    public Integer getId() { return this.id; }
    public UniqueDigitKey getKey() {
        if (this.key != null) {
            return this.key;
        }
        return new UniqueDigitKey(this.numericSequence, this.multiplier);
    }


    private String reduceToDigit (String sequence) {

        var length = sequence.length();
        if (length == 1) {
            return sequence;
        }

        Integer digit = 0;
        for (int i = 0; i < length ; i++) {
            var currentChar = String.valueOf(sequence.charAt(i));
            digit += Integer.parseInt(currentChar);
        }

        return reduceToDigit(String.valueOf(digit));
    }

    public void doCalculate() {
        var completeNumber = UniqueDigitUtils.repeatNumber(key);
        this.calculatedDigit = Integer.parseInt(reduceToDigit(completeNumber));
    }

    public void setNumericSequence(String numericSequence) {
        this.numericSequence = numericSequence;
    }

    public void setMultiplier(Integer multiplier) {
        this.multiplier = multiplier;
    }
}
