package br.com.maggi.uniquedigit.core;

import br.com.maggi.uniquedigit.user.User;
import br.com.maggi.uniquedigit.user.UserRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/uniqueDigits")
@Api("Unique Digits")
public class UniqueDigitController {

    @Autowired
    UniqueDigitService service;

    @Autowired
    UserRepository userRepository;

    @Autowired
    UniqueDigitRepository digitRepository;


    @PostMapping
    @ApiOperation("Calculate a Unique Digit from Number and Multiplier. Optional: Pass a user id to persist the calculation")
    @ApiResponses({
            @ApiResponse(code = 400, message = "Input parameters are invalid"),
            @ApiResponse(code = 404, message = "User Id received to register, but was not found"),
            @ApiResponse(code = 200, message = "Unique Digits successfully calculated"),
    })
    public ResponseEntity Calculate(@RequestBody UniqueDigit input, @RequestParam(required = false) Integer userId) {

        UniqueDigit digit;

        try {
            digit = GetUniqueDigit(input);
        } catch (IllegalArgumentException ex) {
            return new ResponseEntity(ex.getMessage(), HttpStatus.BAD_REQUEST);
        }

        if (userId != null) {
            try {
                UpdateUserDigits(digit, userId);
            } catch (NotFoundException ex) {
                return new ResponseEntity(ex.getMessage(), HttpStatus.NOT_FOUND);
            }
        }

        return new ResponseEntity(digit, HttpStatus.OK);

    }

    private UniqueDigit GetUniqueDigit (UniqueDigit input) {
        String numericSequence = input.getNumericSequence();
        Integer multiplier = input.getMultiplier();

        var digit = digitRepository.findByNumericSequenceAndMultiplier(numericSequence, multiplier);

        if (digit == null) {
            var calculatedDigit = (UniqueDigit)service.Calculate(numericSequence, multiplier);
            digit = digitRepository.save(calculatedDigit);
        }

        return digit;
    }

    private void UpdateUserDigits (UniqueDigit digit, Integer userId) throws NotFoundException {
        User user = userRepository.findById(userId).orElse(null);

        if (user == null) {
            throw new NotFoundException(String.format("User Id %s does not exists!", userId));
        }

        user.AddUniqueDigit(digit);
        userRepository.save(user);

    }


}
