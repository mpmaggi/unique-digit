package br.com.maggi.uniquedigit.core;

import br.com.maggi.uniquedigit.cache.Cache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UniqueDigitService {

    @Autowired
    Cache cache;

    public UniqueDigitService() {}

    public IUniqueDigit Calculate (String numberSequence, Integer multiplier) {
        UniqueDigit digitOnCache = cache.Get(numberSequence, multiplier);

        if (digitOnCache != null) {
            return digitOnCache;
        }

        UniqueDigit digit = new UniqueDigit(numberSequence, multiplier);
        cache.Add(digit);
        return digit;
    }
}
