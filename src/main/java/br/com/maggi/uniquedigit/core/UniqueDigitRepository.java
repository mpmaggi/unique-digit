package br.com.maggi.uniquedigit.core;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UniqueDigitRepository extends JpaRepository<UniqueDigit, Integer> {

    public UniqueDigit findByNumericSequenceAndMultiplier(String numericSequence, Integer multiplier);

}
