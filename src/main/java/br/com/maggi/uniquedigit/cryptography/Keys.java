package br.com.maggi.uniquedigit.cryptography;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.security.PrivateKey;
import java.security.PublicKey;

@Entity
@Table(name = "keys")
public class Keys {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Integer id;

    @Column(name = "public_key")
    @JsonIgnore
    private PublicKey publicKey;

    @Column(name = "private_key")
    @JsonIgnore
    private PrivateKey privateKey;

    @Transient
    private String token;

    public Keys() {
        var keyPair = Cryptography.GenerateKeys();
        this.publicKey = keyPair.getPublic();
        this.privateKey = keyPair.getPrivate();
        this.token = Cryptography.KeyToBase64(this.publicKey);
    };


    public PublicKey getPublicKey() {
        return publicKey;
    }

    public PrivateKey getPrivateKey() {
        return privateKey;
    }

    public String getToken() {
        return token;
    }

}
