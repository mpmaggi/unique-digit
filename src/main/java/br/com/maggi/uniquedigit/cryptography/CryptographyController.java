package br.com.maggi.uniquedigit.cryptography;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/keys")
@Api("Cryptography")
public class CryptographyController {

    @Autowired
    CryptographyRepository repository;

    @PostMapping
    @ApiOperation("Generates new token (public key) for a user")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Public and Private Key successfully created", response = Keys.class),
            @ApiResponse(code = 500, message = "Error on generating keys"),
    })
    public ResponseEntity generateNewKeys () {
        try {
            Keys keys = new Keys();
            repository.save(keys);
            return new ResponseEntity(keys, HttpStatus.CREATED);
        } catch (Exception ex) {
            return new ResponseEntity(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
