package br.com.maggi.uniquedigit.cryptography;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CryptographyRepository extends JpaRepository<Keys, Integer> {
}
