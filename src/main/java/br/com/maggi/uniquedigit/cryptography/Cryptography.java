package br.com.maggi.uniquedigit.cryptography;

import javax.crypto.Cipher;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

import static java.nio.charset.StandardCharsets.UTF_8;

public class Cryptography {
    public static KeyPair GenerateKeys() {
        KeyPair keyPair = null;

        try {
            KeyPairGenerator keyGenerator = KeyPairGenerator.getInstance("RSA");
            keyGenerator.initialize(2048, new SecureRandom());
            keyPair = keyGenerator.generateKeyPair();
        } catch (Exception ex) {
            System.out.println("Invalid Algorithm for KeyPair");
        }
        return keyPair;
    }

    public static String Encrypt(String input, PublicKey pubKey) {
        byte[] encryptText = null;
        try {
            Cipher encrypt = Cipher.getInstance("RSA");
            encrypt.init(Cipher.ENCRYPT_MODE, pubKey);

            encryptText = encrypt.doFinal(input.getBytes(UTF_8));

        } catch (Exception ex) {
            System.out.println("Invalid Algorithm for encryption");
        }

        var encrypted =Base64.getEncoder().encodeToString(encryptText);

        return encrypted;
    }

    public static String Decrypt(String encryptedInput, PrivateKey privKey) {
        Cipher decrypt = null;
        byte[] bytes = null;
        String decryptedText = null;

        try {
            bytes = Base64.getDecoder().decode(encryptedInput);
            decrypt = Cipher.getInstance("RSA");
            decrypt.init(Cipher.DECRYPT_MODE, privKey);
            decryptedText = new String(decrypt.doFinal(bytes), UTF_8);
        } catch (Exception ex) {
            System.out.println("Invalid Algorithm for decryption");
        }

        return decryptedText;
    }

    public static String KeyToBase64(PublicKey key) {
        return new String(Base64.getEncoder().encode(key.getEncoded()), StandardCharsets.UTF_8);
    }

    public static PublicKey Base64ToPublicKey(String token) {
        PublicKey pubKey = null;

        try {
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            byte[] b = Base64.getDecoder().decode(token.getBytes(StandardCharsets.UTF_8));
            pubKey = keyFactory.generatePublic(new X509EncodedKeySpec(b));
        } catch (Exception ex) {
           System.out.println("Error on converting token to public key");
        }
        return pubKey;
    }
}
